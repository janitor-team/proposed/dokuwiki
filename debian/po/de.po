# translation of dokuwiki_de.po to German
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans#
#    Developers do not need to manually edit POT or PO files.
#
# Erik Schanze <eriks@debian.org>, 2006-2010.
# Holger Wansing <linux@wansing-online.de>, 2010-2013.
#
msgid ""
msgstr ""
"Project-Id-Version: dokuwiki_0.0.20130510a-2\n"
"Report-Msgid-Bugs-To: dokuwiki@packages.debian.org\n"
"POT-Creation-Date: 2013-10-27 19:00+0100\n"
"PO-Revision-Date: 2013-11-04 21:12+0100\n"
"Last-Translator: Holger Wansing <linux@wansing-online.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lighttpd"
msgstr "lighttpd"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid "Web server(s) to configure automatically:"
msgstr "Automatisch einzurichtende(r) Webserver:"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"DokuWiki runs on any web server supporting PHP, but only listed web servers "
"can be configured automatically."
msgstr ""
"DokuWiki läuft auf jedem Webserver, der PHP unterstützt, aber nur die hier "
"aufgeführten können automatisch eingerichtet werden."

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"DokuWiki."
msgstr ""
"Bitte wählen Sie den oder die Webserver aus, die automatisch für DokuWiki "
"eingerichtet werden sollen."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the web server(s) be restarted now?"
msgstr "Webserver jetzt neu starten?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"In order to activate the new configuration, the reconfigured web server(s) "
"have to be restarted."
msgstr ""
"Damit die neuen Einstellungen greifen können, müssen alle umkonfigurierten "
"Webserver neu gestartet werden."

#. Type: string
#. Description
#: ../templates:3001
msgid "Wiki location:"
msgstr "Wiki-Speicherort:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Specify the directory below the server's document root from which DokuWiki "
"should be accessible."
msgstr ""
"Bitte das Verzeichnis unterhalb des »Dokument-Root« des Webservers eingeben, "
"in dem DokuWiki erreichbar sein soll."

#. Type: select
#. Description
#: ../templates:4001
msgid "Authorized network:"
msgstr "Zugelassenes Netzwerk:"

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"Wikis normally provide open access to their content, allowing anyone to "
"modify it. Alternatively, access can be restricted by IP address."
msgstr ""
"Wikis bieten normalerweise offenen Zugang zu ihrem Inhalt und erlauben "
"jedem, ihn zu ändern. Der Zugang kann aber auch auf der Basis der IP-Adresse "
"eingeschränkt werden."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"If you select \"localhost only\", only people on the local host (the machine "
"the wiki is running on) will be able to connect. \"local network\" will "
"allow people on machines in a local network (which you will need to specify) "
"to talk to the wiki. \"global\" will allow anyone, anywhere, to connect to "
"the wiki."
msgstr ""
"Wenn Sie »nur lokaler Rechner« wählen, können sich nur Benutzer verbinden, "
"die direkt an dem Rechner arbeiten, auf dem das Wiki läuft. »Lokales "
"Netzwerk« erlaubt es, von den Rechnern des lokalen Netzwerks (das Sie "
"angeben müssen) mit dem Wiki zu arbeiten. »Alle« bedeutet, dass sich jeder "
"von überall mit dem Wiki verbinden kann."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"The default is for site security, but more permissive settings should be "
"safe unless you have a particular need for privacy."
msgstr ""
"Die Voreinstellung dient der Sicherheit der Site, aber offenere "
"Einstellungen sind auch sicher, solange Sie keine besonderen Anforderungen "
"an Geheimhaltung haben."

#. Type: select
#. Choices
#: ../templates:4002
msgid "localhost only"
msgstr "nur lokaler Rechner"

#. Type: select
#. Choices
#: ../templates:4002
msgid "local network"
msgstr "lokales Netzwerk"

#. Type: select
#. Choices
#: ../templates:4002
msgid "global"
msgstr "alle"

#. Type: string
#. Description
#: ../templates:5001
msgid "Local network:"
msgstr "Lokales Netzwerk:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The specification of your local network should either be an IP network in "
"CIDR format (x.x.x.x/y) or a domain specification (like .example.com)."
msgstr ""
"Die Angabe Ihres lokalen Netzwerks sollte eine IP-Netzwerkadresse im CIDR-"
"Format (x.x.x.x/y) oder ein Domänenname (wie .example.com) sein."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Anyone who matches this specification will be given full and complete access "
"to DokuWiki's content."
msgstr ""
"Jeder, der die unten eingegebene Vorgabe erfüllt, erhält vollständigen "
"Zugang zu DokuWikis Inhalt."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Purge pages on package removal?"
msgstr "Seiten beim vollständigen Entfernen des Pakets löschen?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"By default, DokuWiki stores all its pages in a file database in /var/lib/"
"dokuwiki."
msgstr ""
"Standardmäßig speichert DokuWiki alle seine Seiten in einer Datei-Datenbank "
"im Verzeichnis /var/lib/dokuwiki."

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Accepting this option will leave you with a tidier system when the DokuWiki "
"package is removed, but may cause information loss if you have an "
"operational wiki that gets removed."
msgstr ""
"Wenn Sie zustimmen, haben Sie ein aufgeräumteres System, falls das Paket "
"DokuWiki vollständig entfernt wird, aber es können Informationen verloren "
"gehen, wenn Sie ein Wiki löschen, das noch benutzt wird."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Make the configuration web-writeable?"
msgstr "Einstellungen für den Webserver schreibbar machen?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"DokuWiki includes a web-based configuration interface. To be usable, it "
"requires the web server to have write permission to the configuration "
"directory."
msgstr ""
"DokuWiki bringt eine web-basierte Konfigurationsoberfläche mit. Damit sie "
"funktioniert, benötigt der Webserver Schreibrechte auf das "
"Konfigurationsverzeichnis."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Accepting this option will give the web server write permissions on the "
"configuration directory and files."
msgstr ""
"Wenn Sie hier zustimmen, erhält der Webserver Schreibrechte auf "
"Konfigarionsverzeichnis und -dateien."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"The configuration files will still be readable and editable by hand "
"regardless of whether or not you accept this option."
msgstr ""
"Die Konfigurationsdateien bleiben lesbar und manuell änderbar, gleichgültig, "
"welche Auswahl Sie treffen."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Make the plugins directory web-writeable?"
msgstr "Verzeichnis der Plugins für den Webserver schreibbar machen?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"DokuWiki includes a web-based plugin installation interface. To be usable, "
"it requires the web server to have write permission to the plugins directory."
msgstr ""
"DokuWiki bringt eine web-basierte Konfigurationsoberfläche mit. Damit sie "
"funktioniert, benötigt der Webserver Schreibrechte auf das Plugins-"
"Verzeichnis."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Accepting this option will give the web server write permissions to the "
"plugins directory."
msgstr ""
"Wenn Sie hier zustimmen, erhält der Webserver Schreibrechte auf das Plugins-"
"Verzeichnis."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Plugins can still be installed by hand regardless of whether or not you "
"accept this option."
msgstr ""
"Plugins können weiterhin manuell installiert werden, gleichgültig, welche "
"Auswahl Sie treffen."

#. Type: string
#. Description
#: ../templates:9001
msgid "Wiki title:"
msgstr "Wiki-Titel:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The wiki title will be displayed in the upper right corner of the default "
"template and on the browser window title."
msgstr ""
"Der Wiki-Titel wird in der oberen rechten Ecke der Standardschablone und in "
"der Titelleiste des Webbrowsers angezeigt."

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC0 \"No Rights Reserved\""
msgstr "CC0 »No Rights Reserved«"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution"
msgstr "CC Attribution"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-ShareAlike"
msgstr "CC Attribution-ShareAlike"

#. Type: select
#. Choices
#: ../templates:10001
msgid "GNU Free Documentation Licence"
msgstr "GNU Free Documentation License"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial"
msgstr "CC Attribution-NonCommercial"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial-ShareAlike"
msgstr "CC Attribution-NonCommercial-ShareAlike"

#. Type: select
#. Description
#: ../templates:10002
msgid "Wiki license:"
msgstr "Wiki-Lizenz:"

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Please choose the license you want to apply to your wiki content. If none of "
"these licenses suits your needs, you will be able to add your own to the "
"file /etc/dokuwiki/license.php and to refer it in the main configuration "
"file /etc/dokuwiki/local.php when the installation is finished."
msgstr ""
"Bitte wählen Sie die Lizenz, die Sie für den Inhalt Ihres Wikis verwenden "
"möchten. Falls keine dieser Lizenzen Ihren Bedürfnissen genügt, können Sie "
"Ihre eigene zur Datei /etc/dokuwiki/license.php hinzufügen und in der "
"Haupt-Konfigurationsdatei /etc/dokuwiki/local.php darauf verweisen, sobald "
"die Installation abgeschlossen ist."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Creative Commons \"No Rights Reserved\" is designed to waive as many rights "
"as legally possible."
msgstr ""
"Creative Commons CC0 »No Rights Reserved« wurde entwickelt, um so viele "
"Rechte wie rechtlich erlaubt auszuschließen."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution is a permissive license that only requires licensees to give "
"credit to the author."
msgstr ""
"CC Attribution ist eine freizügige Lizenz, die vom Lizenznehmer lediglich "
"erfordert, dem Autor Anerkennung für seine Arbeit zu zollen."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-ShareAlike and GNU Free Documentation License are copyleft-"
"based free licenses (requiring modifications to be released under similar "
"terms)."
msgstr ""
"CC Attribution-ShareAlike und GNU Free Documentation License sind Copyleft-"
"basierte, freie Lizenzen (sie erfordern, dass Änderungen unter ähnlichen "
"Bedingungen veröffentlicht werden)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike are "
"non-free licenses, in that they forbid commercial use."
msgstr ""
"CC Attribution-NonCommercial und CC Attribution-Noncommercial-ShareAlike "
"sind nicht-freie Lizenzen, die kommerzielle Nutzung verbieten."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Enable ACL?"
msgstr "ACL aktivieren?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"Enable this to use an Access Control List for restricting what the users of "
"your wiki may do."
msgstr ""
"Wenn Sie dies aktivieren, wird eine Access-Control-List (ACL) verwendet, um "
"einschränken zu können, was die Benutzer Ihres Wikis dürfen."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"This is a recommended setting because without ACL support you will not have "
"access to the administration features of DokuWiki."
msgstr ""
"Diese Einstellung wird empfohlen, da Sie ohne Unterstützung für eine Access-"
"Control-List keinen Zugriff auf die Administrationsfunktionen von DokuWiki "
"haben werden."

#. Type: string
#. Description
#: ../templates:12001
msgid "Administrator username:"
msgstr "Benutzername des Administrators:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please enter a name for the administrator account, which will be able to "
"manage DokuWiki's configuration and create new wiki users. The username "
"should be composed of lowercase ASCII letters only."
msgstr ""
"Bitte geben Sie einen Namen für den Administratorzugang ein, der die "
"DokuWiki-Konfiguration verwalten und neue Wiki-Benutzer anlegen kann. Der "
"Benutzername sollte nur aus Kleinbuchstaben aus dem ASCII-Zeichensatz "
"bestehen."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"If this field is left blank, no administrator account will be created now."
msgstr ""
"Falls Sie dieses Feld leer lassen, wird jetzt kein Administratorzugang "
"erstellt."

#. Type: string
#. Description
#: ../templates:13001
msgid "Administrator real name:"
msgstr "Echter Name des Administrators:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please enter the full name associated with the wiki administrator account. "
"This name will be stored in the wiki password file as an informative field, "
"and will be displayed with the wiki page changes made by the administrator "
"account."
msgstr ""
"Bitte geben Sie den vollständigen Namen ein, der zu dem Administratorzugang "
"gehört. Dieser Name wird in der Wiki-Passwortdatei als informatives Feld "
"abgespeichert und bei den Änderungen an den Wiki-Seiten angezeigt, die durch "
"den Administator durchgeführt wurden."

#. Type: string
#. Description
#: ../templates:14001
msgid "Administrator email address:"
msgstr "E-Mail-Adresse des Administrators:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please enter the email address associated with the wiki administrator "
"account. This address will be stored in the wiki password file, and may be "
"used to get a new administrator password if you lose the original."
msgstr ""
"Bitte geben Sie die E-Mail-Adresse ein, die zu dem Administratorzugang "
"gehört. Diese Adresse wird in der Wiki-Passwortdatei abgespeichert und kann "
"genutzt werden, um ein neues Administratorpasswort zu erhalten, falls das "
"Original-Passwort verloren/vergessen wurde."

#. Type: password
#. Description
#: ../templates:15001
msgid "Administrator password:"
msgstr "Passwort des Administrators:"

#. Type: password
#. Description
#: ../templates:15001
msgid "Please choose a password for the wiki administrator."
msgstr "Bitte geben Sie ein Passwort für den Wiki-Administrator ein."

#. Type: password
#. Description
#: ../templates:16001
msgid "Re-enter password to verify:"
msgstr "Passwort zur Kontrolle erneut eingeben:"

#. Type: password
#. Description
#: ../templates:16001
msgid ""
"Please enter the same \"admin\" password again to verify you have typed it "
"correctly."
msgstr ""
"Bitte geben Sie das gleiche Administrator-Passwort erneut ein, um "
"sicherzustellen, dass Sie sich nicht vertippt haben."

#. Type: error
#. Description
#: ../templates:17001
msgid "Password input error"
msgstr "Passwort-Eingabefehler"

#. Type: error
#. Description
#: ../templates:17001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"Die beiden Passwörter, die Sie eingegeben haben, sind nicht identisch. Bitte "
"versuchen Sie es erneut."

#. Type: select
#. Description
#: ../templates:18001
msgid "Initial ACL policy:"
msgstr "Anfängliche ACL-Richtlinie:"

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"Please select what initial ACL configuration should be set up to match the "
"intended usage of this wiki:\n"
" \"open\":   both readable and writeable for anonymous users;\n"
" \"public\": readable for anonymous users, writeable for registered users;\n"
" \"closed\": readable and writeable for registered users only."
msgstr ""
"Bitte wählen Sie aus, welche anfängliche ACL-Konfiguration, die für die "
"beabsichtigte Nutzung des Wikis passend ist, eingestellt werden soll:\n"
" »offen«:       sowohl lesbar wie auch schreibbar für anonyme Benutzer;\n"
" »öffentlich«:  lesbar für anonyme Benutzer, schreibbar nur für "
"registrierte\n"
"                Benutzer;\n"
" »geschlossen«: lesbar und schreibbar nur für registrierte Benutzer."

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"This is only an initial setup; you will be able to adjust the ACL rules "
"later."
msgstr ""
"Dies ist nur eine anfängliche Einstellung, Sie können die ACL-Regeln später "
"noch anpassen."

#. Type: select
#. Choices
#: ../templates:18002
msgid "open"
msgstr "offen"

#. Type: select
#. Choices
#: ../templates:18002
msgid "public"
msgstr "öffentlich"

#. Type: select
#. Choices
#: ../templates:18002
msgid "closed"
msgstr "geschlossen"

#~ msgid "Please choose the license you want to apply to your wiki content."
#~ msgstr ""
#~ "Bitte wählen Sie die Lizenz, die Sie für den Inhalt Ihres Wikis verwenden "
#~ "möchten."
